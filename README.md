# Setup for lab session
Follow these instructions on Ubuntu 18.04 to setup Jupyter Notebook, PySpark and data science libraries.


## System requirements

```
sudo apt update
sudo apt install python3 python3-pip python3-virtualenv openjdk-8-jdk
```

If installed, remove Java 11 (Spark needs Java 8):
```
sudo apt remove openjdk-11-jdk openjdk-11-jre openjdk-11-jdk-headless openjdk-11-jre-headless
```


## Setup Python virtual environment
Setup the virtual environment (for instance in your home directory).
Replace `venv` with any name relevant for you.
```
virtualenv venv --python=python3
```

Activate the virtual environment (replace `venv` with the name you chose):
```
source venv/bin/activate
```
You need to do this every time you launch a new terminal.


## Python packages
Make sure the virtual environment is activated, and install these packages:
```
pip install numpy scipy jupyter matplotlib pandas pyspark sklearn
```


## Run
Make sure the virtual environment is activated, and aunch a notebook (it should open a brower window):
```
jupyter notebook
```


# Test installation
In the Jupyter notebook (browser window), choose `New` and `Python 3`.
Type in the cell:
```
import pyspark as ps
sc = ps.SparkContext()
sc.parallelize(range(200)).mean()
```
Validate with `CTRL`+`Enter`.

It should output the result: `99.5`
